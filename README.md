# generator-habitat
> Yeoman generator for [Sitecore Habitat](https://github.com/Sitecore/Habitat)

> Modified by [Matt Gianino](mailto:mgianino@r2integrated.com) for use on R2i Sitecore projects. Adds some additional packages for bundling and minifying as well as referencing the proper Sitecore nuget packages.

## Installation

First, install [Yeoman](http://yeoman.io) using [npm](https://www.npmjs.com/) (we assume you have pre-installed [node.js](https://nodejs.org/)).

```bash
npm install -g yo
```

## Adding this fork to local npm

Clone this repository locally.  In order to use this fork locally, you need to register it with your local npm since it does not exist in the public npm repository. 

To do this, navigate into the `generators\feature` folder and run `npm link`.

```bash
$ cd generators/feature/
$ npm link
npm WARN prepublish-on-install As of npm@5, `prepublish` scripts will run only for `npm publish`.
npm WARN prepublish-on-install (In npm@4 and previous versions, it also runs for `npm install`.)
npm WARN prepublish-on-install See the deprecation note in `npm help scripts` for more information.

> generator-habitat@0.0.8 prepublish C:\Dev\Repo\Yao
> gulp prepublish

[17:02:06] Using gulpfile C:\Dev\Repo\Yao\gulpfile.js
[17:02:06] Starting 'nsp'...
[17:02:06] (+) No known vulnerabilities found
[17:02:06] Finished 'nsp' after 501 ms
[17:02:06] Starting 'prepublish'...
[17:02:06] Finished 'prepublish' after 4.21 μs
```

This will, in essence, 'install' the generator locally on your machine so you can reference it using `yo` from anywhere.

## Installing Habitat

This fork assumes you already have a working installation of Habitat.

## Adding Features to Habitat

This **must** be done in the ***root*** of your specific project installation directory, on the same level as the `src` folder (example: `C:\Dev\Repo\Sutherland`).

Execute in the root of your existing Habitat/Project installation:

```bash
yo habitat:feature
```
...then follow the prompts.

This will add a full feature to your src/Features folder. From there you must import the newly-created .csproj file into your Visual Studio solution.

## Adding Foundation Modules to Habitat

Adding foundation modules is not yet fully supported in this fork.

## License

Apache-2.0


[npm-image]: https://badge.fury.io/js/generator-habitat.svg
[npm-url]: https://npmjs.org/package/generator-habitat